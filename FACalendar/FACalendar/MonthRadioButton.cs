﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FACalendar
{
    class MonthRadioButton : RadioButton
    {
        public int Month { get { return _month; } set { _month = value; RefreshText(); } }
        private int _month;

        public MonthRadioButton(int month)
        {
            Month = month;
        }

        public void RefreshText()
        {
            this.Text = StringExtension.MonthName(_month);
        }
    }
}
