﻿using System;
using System.Windows.Forms;

namespace FACalendar
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.buttonPreview = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButtonMonth12 = new System.Windows.Forms.RadioButton();
            this.radioButtonMonth11 = new System.Windows.Forms.RadioButton();
            this.radioButtonMonth10 = new System.Windows.Forms.RadioButton();
            this.radioButtonMonth09 = new System.Windows.Forms.RadioButton();
            this.radioButtonMonth08 = new System.Windows.Forms.RadioButton();
            this.radioButtonMonth07 = new System.Windows.Forms.RadioButton();
            this.radioButtonMonth06 = new System.Windows.Forms.RadioButton();
            this.radioButtonMonth05 = new System.Windows.Forms.RadioButton();
            this.radioButtonMonth04 = new System.Windows.Forms.RadioButton();
            this.radioButtonMonth03 = new System.Windows.Forms.RadioButton();
            this.radioButtonMonth02 = new System.Windows.Forms.RadioButton();
            this.radioButtonMonth01 = new System.Windows.Forms.RadioButton();
            this.buttonPrint = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonPreview
            // 
            this.buttonPreview.Location = new System.Drawing.Point(120, 139);
            this.buttonPreview.Name = "buttonPreview";
            this.buttonPreview.Size = new System.Drawing.Size(151, 36);
            this.buttonPreview.TabIndex = 1;
            this.buttonPreview.Text = "Preview";
            this.buttonPreview.UseVisualStyleBackColor = true;
            this.buttonPreview.Click += new System.EventHandler(this.buttonPrintPreview_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(616, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "label1";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButtonMonth12);
            this.groupBox1.Controls.Add(this.radioButtonMonth11);
            this.groupBox1.Controls.Add(this.radioButtonMonth10);
            this.groupBox1.Controls.Add(this.radioButtonMonth09);
            this.groupBox1.Controls.Add(this.radioButtonMonth08);
            this.groupBox1.Controls.Add(this.radioButtonMonth07);
            this.groupBox1.Controls.Add(this.radioButtonMonth06);
            this.groupBox1.Controls.Add(this.radioButtonMonth05);
            this.groupBox1.Controls.Add(this.radioButtonMonth04);
            this.groupBox1.Controls.Add(this.radioButtonMonth03);
            this.groupBox1.Controls.Add(this.radioButtonMonth02);
            this.groupBox1.Controls.Add(this.radioButtonMonth01);
            this.groupBox1.Location = new System.Drawing.Point(742, 67);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 306);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Monat";
            // 
            // radioButtonMonth12
            // 
            this.radioButtonMonth12.AutoSize = true;
            this.radioButtonMonth12.Location = new System.Drawing.Point(19, 275);
            this.radioButtonMonth12.Name = "radioButtonMonth12";
            this.radioButtonMonth12.Size = new System.Drawing.Size(74, 17);
            this.radioButtonMonth12.TabIndex = 11;
            this.radioButtonMonth12.Text = "December";
            this.radioButtonMonth12.UseVisualStyleBackColor = true;
            this.radioButtonMonth12.CheckedChanged += new System.EventHandler(this.monthRadioButtonsCheckedChanged);
            // 
            // radioButtonMonth11
            // 
            this.radioButtonMonth11.AutoSize = true;
            this.radioButtonMonth11.Location = new System.Drawing.Point(19, 252);
            this.radioButtonMonth11.Name = "radioButtonMonth11";
            this.radioButtonMonth11.Size = new System.Drawing.Size(74, 17);
            this.radioButtonMonth11.TabIndex = 10;
            this.radioButtonMonth11.Text = "November";
            this.radioButtonMonth11.UseVisualStyleBackColor = true;
            this.radioButtonMonth11.CheckedChanged += new System.EventHandler(this.monthRadioButtonsCheckedChanged);
            // 
            // radioButtonMonth10
            // 
            this.radioButtonMonth10.AutoSize = true;
            this.radioButtonMonth10.Location = new System.Drawing.Point(19, 229);
            this.radioButtonMonth10.Name = "radioButtonMonth10";
            this.radioButtonMonth10.Size = new System.Drawing.Size(63, 17);
            this.radioButtonMonth10.TabIndex = 9;
            this.radioButtonMonth10.Text = "October";
            this.radioButtonMonth10.UseVisualStyleBackColor = true;
            this.radioButtonMonth10.CheckedChanged += new System.EventHandler(this.monthRadioButtonsCheckedChanged);
            // 
            // radioButtonMonth09
            // 
            this.radioButtonMonth09.AutoSize = true;
            this.radioButtonMonth09.Location = new System.Drawing.Point(19, 206);
            this.radioButtonMonth09.Name = "radioButtonMonth09";
            this.radioButtonMonth09.Size = new System.Drawing.Size(76, 17);
            this.radioButtonMonth09.TabIndex = 8;
            this.radioButtonMonth09.Text = "September";
            this.radioButtonMonth09.UseVisualStyleBackColor = true;
            this.radioButtonMonth09.CheckedChanged += new System.EventHandler(this.monthRadioButtonsCheckedChanged);
            // 
            // radioButtonMonth08
            // 
            this.radioButtonMonth08.AutoSize = true;
            this.radioButtonMonth08.Location = new System.Drawing.Point(19, 183);
            this.radioButtonMonth08.Name = "radioButtonMonth08";
            this.radioButtonMonth08.Size = new System.Drawing.Size(58, 17);
            this.radioButtonMonth08.TabIndex = 7;
            this.radioButtonMonth08.Text = "August";
            this.radioButtonMonth08.UseVisualStyleBackColor = true;
            this.radioButtonMonth08.CheckedChanged += new System.EventHandler(this.monthRadioButtonsCheckedChanged);
            // 
            // radioButtonMonth07
            // 
            this.radioButtonMonth07.AutoSize = true;
            this.radioButtonMonth07.Location = new System.Drawing.Point(19, 160);
            this.radioButtonMonth07.Name = "radioButtonMonth07";
            this.radioButtonMonth07.Size = new System.Drawing.Size(43, 17);
            this.radioButtonMonth07.TabIndex = 6;
            this.radioButtonMonth07.Text = "July";
            this.radioButtonMonth07.UseVisualStyleBackColor = true;
            this.radioButtonMonth07.CheckedChanged += new System.EventHandler(this.monthRadioButtonsCheckedChanged);
            // 
            // radioButtonMonth06
            // 
            this.radioButtonMonth06.AutoSize = true;
            this.radioButtonMonth06.Location = new System.Drawing.Point(19, 137);
            this.radioButtonMonth06.Name = "radioButtonMonth06";
            this.radioButtonMonth06.Size = new System.Drawing.Size(48, 17);
            this.radioButtonMonth06.TabIndex = 5;
            this.radioButtonMonth06.Text = "June";
            this.radioButtonMonth06.UseVisualStyleBackColor = true;
            this.radioButtonMonth06.CheckedChanged += new System.EventHandler(this.monthRadioButtonsCheckedChanged);
            // 
            // radioButtonMonth05
            // 
            this.radioButtonMonth05.AutoSize = true;
            this.radioButtonMonth05.Location = new System.Drawing.Point(19, 114);
            this.radioButtonMonth05.Name = "radioButtonMonth05";
            this.radioButtonMonth05.Size = new System.Drawing.Size(45, 17);
            this.radioButtonMonth05.TabIndex = 4;
            this.radioButtonMonth05.Text = "May";
            this.radioButtonMonth05.UseVisualStyleBackColor = true;
            this.radioButtonMonth05.CheckedChanged += new System.EventHandler(this.monthRadioButtonsCheckedChanged);
            // 
            // radioButtonMonth04
            // 
            this.radioButtonMonth04.AutoSize = true;
            this.radioButtonMonth04.Location = new System.Drawing.Point(19, 91);
            this.radioButtonMonth04.Name = "radioButtonMonth04";
            this.radioButtonMonth04.Size = new System.Drawing.Size(45, 17);
            this.radioButtonMonth04.TabIndex = 3;
            this.radioButtonMonth04.Text = "April";
            this.radioButtonMonth04.UseVisualStyleBackColor = true;
            this.radioButtonMonth04.CheckedChanged += new System.EventHandler(this.monthRadioButtonsCheckedChanged);
            // 
            // radioButtonMonth03
            // 
            this.radioButtonMonth03.AutoSize = true;
            this.radioButtonMonth03.Location = new System.Drawing.Point(19, 68);
            this.radioButtonMonth03.Name = "radioButtonMonth03";
            this.radioButtonMonth03.Size = new System.Drawing.Size(55, 17);
            this.radioButtonMonth03.TabIndex = 2;
            this.radioButtonMonth03.Text = "March";
            this.radioButtonMonth03.UseVisualStyleBackColor = true;
            this.radioButtonMonth03.CheckedChanged += new System.EventHandler(this.monthRadioButtonsCheckedChanged);
            // 
            // radioButtonMonth02
            // 
            this.radioButtonMonth02.AutoSize = true;
            this.radioButtonMonth02.Location = new System.Drawing.Point(19, 45);
            this.radioButtonMonth02.Name = "radioButtonMonth02";
            this.radioButtonMonth02.Size = new System.Drawing.Size(66, 17);
            this.radioButtonMonth02.TabIndex = 1;
            this.radioButtonMonth02.Text = "February";
            this.radioButtonMonth02.UseVisualStyleBackColor = true;
            this.radioButtonMonth02.CheckedChanged += new System.EventHandler(this.monthRadioButtonsCheckedChanged);
            // 
            // radioButtonMonth01
            // 
            this.radioButtonMonth01.AutoSize = true;
            this.radioButtonMonth01.Checked = true;
            this.radioButtonMonth01.Location = new System.Drawing.Point(19, 22);
            this.radioButtonMonth01.Name = "radioButtonMonth01";
            this.radioButtonMonth01.Size = new System.Drawing.Size(62, 17);
            this.radioButtonMonth01.TabIndex = 0;
            this.radioButtonMonth01.TabStop = true;
            this.radioButtonMonth01.Text = "January";
            this.radioButtonMonth01.UseVisualStyleBackColor = true;
            this.radioButtonMonth01.CheckedChanged += new System.EventHandler(this.monthRadioButtonsCheckedChanged);
            // 
            // buttonPrint
            // 
            this.buttonPrint.Location = new System.Drawing.Point(120, 217);
            this.buttonPrint.Name = "buttonPrint";
            this.buttonPrint.Size = new System.Drawing.Size(151, 36);
            this.buttonPrint.TabIndex = 4;
            this.buttonPrint.Text = "Print";
            this.buttonPrint.UseVisualStyleBackColor = true;
            this.buttonPrint.Click += new System.EventHandler(this.buttonPrint_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "yyyy";
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(448, 135);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.ShowUpDown = true;
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 5;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // radioButton441
            // 
            //this.radioButton441.AutoSize = true;
            //this.radioButton441.Location = new System.Drawing.Point(449, 282);
            //this.radioButton441.Name = "radioButton441";
            //this.radioButton441.Size = new System.Drawing.Size(74, 17);
            //this.radioButton441.TabIndex = 12;
            //this.radioButton441.Text = "December";
            //this.radioButton441.UseVisualStyleBackColor = true;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(973, 580);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.buttonPrint);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonPreview);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Main";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Main_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonPreview;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButtonMonth03;
        private System.Windows.Forms.RadioButton radioButtonMonth02;
        private System.Windows.Forms.RadioButton radioButtonMonth01;
        private System.Windows.Forms.RadioButton radioButtonMonth12;
        private System.Windows.Forms.RadioButton radioButtonMonth11;
        private System.Windows.Forms.RadioButton radioButtonMonth10;
        private System.Windows.Forms.RadioButton radioButtonMonth09;
        private System.Windows.Forms.RadioButton radioButtonMonth08;
        private System.Windows.Forms.RadioButton radioButtonMonth07;
        private System.Windows.Forms.RadioButton radioButtonMonth06;
        private System.Windows.Forms.RadioButton radioButtonMonth05;
        private System.Windows.Forms.RadioButton radioButtonMonth04;
        private System.Windows.Forms.Button buttonPrint;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
    }
}

