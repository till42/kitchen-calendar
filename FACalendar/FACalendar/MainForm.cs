﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using System.Threading;
using System.Drawing.Printing;
using FACalendar;

namespace FACalendar {
    public partial class Main : Form {
        private KitchenCalendarPage kitchenCalendarPage; //here we will gather all print-related information
        private PrintPreviewDialog printPreviewDialog1 = new PrintPreviewDialog(); // the preview dialog
        private PrintDocument printDocument1 = new PrintDocument();

        //Properties of all user-input
        private int SelectedMonth {
            get { return _selectedMonth; }
            set {
                if (value < 1 || value > 12) {
                    throw new ArgumentOutOfRangeException();
                }

                _selectedMonth = value;

                //transfer it to KitchenCalendarPage
                kitchenCalendarPage.SelectedMonth = _selectedMonth;
                Console.WriteLine(string.Format("changed SelectMonth={0}", _selectedMonth));
            }
        }
        private int _selectedMonth;

        private DateTime SelectedYear {
            get { return _selectedYear; }
            set {
                _selectedYear = value;
                kitchenCalendarPage.SelectedYear = _selectedYear.Year;
                Console.WriteLine(string.Format("changed SelectYear={0}", _selectedYear.Year));
            }
        }
        private DateTime _selectedYear;

        //SelectedCulture is the selected is directly set
        private string SelectedCulture {
            get { return _culture; }
            set {
                if (value != "") {
                    _culture = value;
                    Thread.CurrentThread.CurrentCulture = new CultureInfo(_culture);
                } else {
                    _culture = Thread.CurrentThread.CurrentCulture.Name;
                }
                Console.WriteLine(string.Format("changed culture={0}", Thread.CurrentThread.CurrentCulture.Name));
            }
        }
        private string _culture;

        public Main() {
            //the whole magic happens in the KitchenCalendarPage class, good to have an instance of it
            kitchenCalendarPage = new KitchenCalendarPage(printDocument1);

            //for explicit German set to "de-DE"
            //leave default culture
            SelectedCulture = "";

            //Initialize the controls
            InitializeComponent();
        }


        //set the initial values
        private void Main_Load(object sender, EventArgs e) {
            //year
            DateTime today = DateTime.Today;
            dateTimePicker1.Value = today;

            //month
            SelectedMonth = today.Month;
        }

        //Preview button was clicked
        private void buttonPrintPreview_Click(object sender, EventArgs e) {
            //prepare and show the preview dialog
            printPreviewDialog1.Document = printDocument1;
            printPreviewDialog1.ShowDialog();
        }

        //Print button was clicked
        private void buttonPrint_Click(object sender, EventArgs e) {
            //do the real printout!
            PrintDocument pd = new PrintDocument();
            pd.PrintPage += new PrintPageEventHandler
               (kitchenCalendarPage.printDocument_PrintPage);
            pd.Print();
        }


        //one of the month-radio buttons was clicked
        private void monthRadioButtonsCheckedChanged(object sender, EventArgs e) {
            //find the radioButton, which was changed
            var radioButton = sender as RadioButton;

            //don't do anything, if the button is not checked
            if (radioButton == null || !radioButton.Checked)
                return;

            //set the month, if we could extract it
            int month;
            if (int.TryParse(radioButton.Name.GetLast(2), out month))
                SelectedMonth = month;
        }

        //the year was changed
        private void dateTimePicker1_ValueChanged(object sender, EventArgs e) {
            SelectedYear = dateTimePicker1.Value;
        }
    }
}
