﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACalendar
{
    public static class StringExtension
    {
        //return the last tail_length characters of a string
        public static string GetLast(this string source, int tail_length)
        {
            if (tail_length >= source.Length)
                return source;
            return source.Substring(source.Length - tail_length);
        }

        //get the name of a month
        public static string MonthName(this int month)
        {
            if (month >= 1 && month <= 12)
                return CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(month);
            else
                return "n/a";
        }
    }
}