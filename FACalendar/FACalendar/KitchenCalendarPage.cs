﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using System.Threading;
using System.Drawing.Printing;
using System.Runtime.InteropServices;
using FACalendar;

namespace FACalendar
{
    public class KitchenCalendarPage
    {
        private readonly PrintDocument _printDocument;
        private PrintPageEventArgs _e;

        private Graphics Graphics => _e.Graphics;

        //user-input will be set by MainForm
        public int SelectedYear { get; set; } //the year for this page
        public int SelectedMonth { get; set; } //the month for this page

        private Pen NormalDayPen { get; } = new Pen(Color.Black, 1f); //Pen for normal day frames
        private Pen HolidayPen { get; } = new Pen(Color.Red, 4f); //Pen for weekend and holiday frames

        private Font TitleFont { get; } =
            new Font(FontFamily.GenericSansSerif, 48.0F, FontStyle.Bold); //font for the title of the page

        private Brush TitleBrush { get; } = Brushes.Black;

        private Font DayOfTheWeekFont { get; } =
            new Font(FontFamily.GenericSansSerif, 40.0F, FontStyle.Bold); //font for each day of the week

        private Brush DayOfTheWeekBrush { get; } = Brushes.Black;

        private Font DateFont { get; } =
            new Font(FontFamily.GenericSansSerif, 30.0f, FontStyle.Regular); //font for the day of a month

        public float HeaderPercentage { get; } =
            1f / 13f; //percentage the header is high as of the total page height

        public float TitlePercentage { get; set; } =
            5f / 10f; //percentage the title is high as of the height of the header

        public float TitleOffsetPercentage { get; set; } =
            2f / 10f; //percentage for the offset after the title as of the height of the header

        public float DayOfTheWeekPercentage { get; set; } =
            3f / 10f; //percentage for the day of week line as of the height of the header

        //useful properties
        private string Title => $"{SelectedMonth.MonthName()} {SelectedYear}";
        private string TitleChinese => $"{SelectedYear}年{SelectedMonth}月";

        //Constructor - we need at least PrintDocument
        public KitchenCalendarPage(PrintDocument printDocument)
        {
            _printDocument = printDocument;
            _printDocument.PrintPage += printDocument_PrintPage;
        }

        //this function is called by the event handler (either Preview or Print)
        public void printDocument_PrintPage(object sender, PrintPageEventArgs e)
        {
            //save the printPageEventArgs into the class
            _e = e;

            #region page_separation

            /* Page
            +--------+-------------------+------------------------------------
            | H      | Title 5/10        | May 2016
            | E   1  | offset 2/10       |
            | A  --  |                   |
            | D  13  | dayOfTheWeek 3/10 | Monday  | Tuesday | Wednesday ....
            +--------+-------------------+---------+---------+----------------
            | B
            | O  12
            | D  --
            | Y  13
            |
            |
            +-----------------
            */

            #endregion

            //calculate sizes

            #region calculate_sizes

            RectangleF pageRect = GetPageRect(e);
            var headerHeight = pageRect.Height * HeaderPercentage;
            var titleHeight = headerHeight * TitlePercentage;
            var offsetHeight = headerHeight * TitleOffsetPercentage;
            var dayOfTheWeekHeight = headerHeight * DayOfTheWeekPercentage;

            var titleRect = new RectangleF(0, 0, pageRect.Width, titleHeight);
            var dayOfTheWeekRect =
                new RectangleF(0, titleHeight + offsetHeight, pageRect.Width, dayOfTheWeekHeight);
            RectangleF dayOfTheMonthRect =
                new RectangleF(0, headerHeight, pageRect.Width, pageRect.Height - headerHeight);

            int dayWidth = Convert.ToInt32(dayOfTheMonthRect.Width / 7); // each week line has 7 days
            int dayHeight = Convert.ToInt32(dayOfTheMonthRect.Height / 6); // there must be room for 6 week lines 
            RectangleF dayRectangleF = new RectangleF(0, 0, dayWidth, dayHeight);

            #endregion

            // Draw Titel
            DrawTitle(titleRect);

            //Draw days of the week
            DrawDaysOfTheWeek(dayOfTheWeekRect);

            //Draw all days of the month
            DrawAllDaysOfMonth(dayOfTheMonthRect, dayRectangleF);
        }

        private RectangleF GetPageRect(PrintPageEventArgs e)
        {
            // If you set printDocument.OriginAtMargins to 'false' this event 
            // will print the largest rectangle your printer is physically 
            // capable of. This is often 1/8" - 1/4" from each page edge.
            // ----------
            // If you set printDocument.OriginAtMargins to 'false' this event
            // will print the largest rectangle permitted by the currently 
            // configured page margins. By default the page margins are 
            // usually 1" from each page edge but can be configured by the end
            // user or overridden in your code.
            // (ex: printDocument.DefaultPageSettings.Margins)

            // Grab a copy of our "soft margins" (configured printer settings)
            // Defaults to 1 inch margins, but could be configured otherwise by 
            // the end user. You can also specify some default page margins in 
            // your printDocument.DefaultPageSetting properties.
            RectangleF marginBounds = e.MarginBounds;

            // Grab a copy of our "hard margins" (printer's capabilities) 
            // This varies between printer models. Software printers like 
            // CutePDF will have no "physical limitations" and so will return 
            // the full page size 850,1100 for a letter page size.
            RectangleF printableArea = e.PageSettings.PrintableArea;


            // If we are print to a print preview control, the origin won't have 
            // been automatically adjusted for the printer's physical limitations. 
            // So let's adjust the origin for preview to reflect the printer's 
            // hard margins.
            //if (printAction == PrintAction.PrintToPreview)
            //    g.TranslateTransform(printableArea.X, printableArea.Y);

            // Are we using soft margins or hard margins? Lets grab the correct 
            // width/height from either the soft/hard margin rectangles. The 
            // hard margins are usually a little wider than the soft margins.
            // ----------
            // Note: Margins are automatically applied to the rotated page size 
            // when the page is set to landscape, but physical hard margins are 
            // not (the printer is not physically rotating any mechanics inside, 
            // the paper still travels through the printer the same way. So we 
            // rotate in software for landscape)
            int availableWidth = (int) Math.Floor(_printDocument.OriginAtMargins
                ? marginBounds.Width
                : (e.PageSettings.Landscape ? printableArea.Height : printableArea.Width));
            int availableHeight = (int) Math.Floor(_printDocument.OriginAtMargins
                ? marginBounds.Height
                : (e.PageSettings.Landscape ? printableArea.Width : printableArea.Height));

            RectangleF rectPage = new RectangleF(0, 0, availableWidth - 1, availableHeight - 1);
            return rectPage;
        }

        //Draw the title (month and year)
        private void DrawTitle(RectangleF rectF)
        {
            Graphics.DrawStringInRect(Title, TitleFont, TitleBrush, rectF, StringAlignment.Near);
            Graphics.DrawStringInRect(TitleChinese, TitleFont, TitleBrush, rectF, StringAlignment.Far);
        }

        //Draw the line with all days of a week
        private void DrawDaysOfTheWeek(RectangleF rectF)
        {
            //Get the maximum sized font to fit onto the page
            Font dayOfTheWeekFont =
                Graphics.MaxSizedFont(CalendarUtilities.AllDaysOfWeekString, DayOfTheWeekFont, rectF);

            //draw each day of the week
            foreach (DayOfWeek dayOfWeek in CalendarUtilities.AllDaysOfWeek)
            {
                int pos = CalendarUtilities.DayOfTheWeekPos(dayOfWeek);
                DrawDayOfWeekTitle(dayOfWeek, dayOfTheWeekFont, rectF, pos);
            }
        }

        //Draw one day of the week
        private void DrawDayOfWeekTitle(DayOfWeek dayOfTheWeek, Font scaledFont, RectangleF rectangleF, int position)
        {
            var dayname = dayOfTheWeek.DayOfWeekName();
            var width = rectangleF.Width / 7;
            var dayRect = new RectangleF(width * position, rectangleF.Y, width, rectangleF.Height);
            Graphics.DrawStringInRect(dayname, scaledFont, DayOfTheWeekBrush, dayRect);
        }

        private void DrawAllDaysOfMonth(RectangleF allDaysRectF, RectangleF singleDayRectangleF)
        {
            #region day_comment

            /*
                    +--------+
                    | 29     |
                    |^offset |
                    |    x%  |
                    |        |
                    |        |
                    |        |
                    +--------+
                        */

            #endregion

            //Preparations
            int numberOfDaysInMonth = DateTime.DaysInMonth(SelectedYear, SelectedMonth);
            Font fontDayScaled = Graphics.MaxSizedFont("29", DateFont,
                new RectangleF(0, 0, singleDayRectangleF.Width / 3, singleDayRectangleF.Height / 3));

            //draw each day of a month
            int line = 0; //week line
            for (int i = 1; i <= numberOfDaysInMonth; i++)
            {
                DateTime day = new DateTime(SelectedYear, SelectedMonth, i); //get the day to be printed

                //calculate the rect for the day
                var rect = new RectangleF(
                    CalendarUtilities.DayOfTheWeekPos(day.DayOfWeek) * singleDayRectangleF.Width,
                    line * singleDayRectangleF.Height + allDaysRectF.Y,
                    singleDayRectangleF.Width,
                    singleDayRectangleF.Height);

                //set the pen
                var dayPen = day.IsHoliday() ? HolidayPen : NormalDayPen;

                //draw the day
                drawDay(day, rect, dayPen, fontDayScaled);

                //check for line break
                if (day.IsLastDayOfWeek())
                {
                    //TODO: draw calendar week https://trello.com/c/VJwdFKTW
                    //    g.DrawCalendarWeek(GetIso8601WeekOfYear(day), rect, fontDayScaled);

                    //next line
                    line++;
                }
            }
        }

        private void drawDay(DateTime day, RectangleF rect, Pen pen, Font dayFont)
        {
            Graphics.DrawRectangle(pen, Rectangle.Round(rect));

            StringFormat stringFormat = new StringFormat();
            stringFormat.Alignment = StringAlignment.Near;
            stringFormat.LineAlignment = StringAlignment.Near;
            Graphics.DrawString(day.Day.ToString(), dayFont, Brushes.Gray, rect);
        }

        public void drawCalendarWeek(Graphics g, int calendarWeek, RectangleF rect, Font dayFont)
        {
            StringFormat stringFormat = new StringFormat();
            stringFormat.Alignment = StringAlignment.Far;
            stringFormat.LineAlignment = StringAlignment.Center;
            g.DrawString("KW" + calendarWeek.ToString(), dayFont, Brushes.Gray, rect, stringFormat);
        }
    }
}