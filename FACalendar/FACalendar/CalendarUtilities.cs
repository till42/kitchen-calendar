﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FACalendar
{
    public static class CalendarUtilities
    {
        public static CultureInfo CurrentCultureInfo { get { return Thread.CurrentThread.CurrentCulture; } }
        public static DayOfWeek FirstDayOfTheWeek { get { return CurrentCultureInfo.DateTimeFormat.FirstDayOfWeek; } }
        public static int DayOfTheWeekPos(DayOfWeek day)
        {
            int dayInt = (int)day;

            //by default the position is set to Sunday as first day (=0), Monday =1, ...
            if (FirstDayOfTheWeek == DayOfWeek.Sunday)
                return dayInt;

            //Monday is the first day of the week in Germany
            if (FirstDayOfTheWeek == DayOfWeek.Monday)
            {
                //first day of the week: 0=Monday, 1=Tuesday, ...
                if (dayInt == 0)
                    dayInt = 7;
                dayInt--;
                return dayInt;
            }

            //so far I don't know of cultures where another day is the first day of the week
            throw new NotImplementedException("Another day as Sunday or Monday is first day of the week.");
        }

        //returns a list of all days in a week as strings, depending on the culture
        public static List<DayOfWeek> AllDaysOfWeek
        {
            get
            {
                var allDaysOfTheWeekList = new List<DayOfWeek>();

                //get all days of the week into a list of string
                foreach (DayOfWeek day in Enum.GetValues(typeof(DayOfWeek)))
                {
                    allDaysOfTheWeekList.Add(day);
                }

                if (FirstDayOfTheWeek == DayOfWeek.Monday)
                {
                    //get the sunday and remove it from the list
                    DayOfWeek sunday = allDaysOfTheWeekList.ElementAt(0);
                    allDaysOfTheWeekList.RemoveAt(0);
                    //add it to the bottom
                    allDaysOfTheWeekList.Add(sunday);
                }
                return allDaysOfTheWeekList;
            }
        }

        public static List<string> AllDaysOfWeekString
        {
            get
            {
                var stringList = new List<string>();
                foreach (DayOfWeek dayOfWeek in AllDaysOfWeek)
                {
                    stringList.Add(dayOfWeek.DayOfWeekName());
                }
                return stringList;
            }
        }


        //return the string of the day of the week ("Montag", "Dienstag", ...)
        public static string DayOfWeekName(this DayOfWeek day)
        {
            return CurrentCultureInfo.DateTimeFormat.GetDayName(day);
        }

        // This presumes that weeks start with Monday.
        // Week 1 is the 1st week of the year with a Thursday in it.
        public static int GetIso8601WeekOfYear(DateTime time)
        {
            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            // Return the week of our adjusted day
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        //is the day a non-working day
        public static bool IsHoliday(this DateTime day)
        {
            //check for a week-end
            if (day.DayOfWeek == DayOfWeek.Saturday || day.DayOfWeek == DayOfWeek.Sunday)
                return true;

            //TODO: Check for holidays https://trello.com/c/0FH4Jtva
            return false;
        }

        public static bool IsLastDayOfWeek(this DateTime day)
        {
            return DayOfTheWeekPos(day.DayOfWeek) == 6;
        }
    }
}
