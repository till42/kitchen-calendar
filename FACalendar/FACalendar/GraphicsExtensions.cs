﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace FACalendar
{
    //Extensions to the System.Drawing.Graphics class that can be used for any project
    public static class GraphicsExtensions
    {
        //Draw a string into a rect, so that it is just as big as it can be. (overload 1 with alignment)
        public static void DrawStringInRect(this Graphics graphics, string text, Font font, Brush brush,
            RectangleF rectangleF,
            StringFormat stringFormat)
        {
            Font maxSizedFont = graphics.MaxSizedFont(text, font, rectangleF);
            graphics.DrawString(text, maxSizedFont, brush, rectangleF, stringFormat);
        }

        //Draw a string into a rect, so that it is just as big as it can be. (overload always left-centered)
        public static void DrawStringInRect(this Graphics graphics, string text, Font font, Brush brush,
            RectangleF rectangleF, StringAlignment alignment = StringAlignment.Center)
        {
            var stringFormat = new StringFormat
            {
                Alignment = alignment,
                LineAlignment = StringAlignment.Center
            };
            graphics.DrawStringInRect(text, font, brush, rectangleF, stringFormat);
        }


        //calculate the maximum font for a string to fit into a rect
        public static Font MaxSizedFont(this Graphics g, string text, Font font, RectangleF rectangleF)
        {
            float ScaleFontSize = MaxFontSize(g, text, font, rectangleF);
            Font maxFont = new Font(font.FontFamily, ScaleFontSize, font.Style, font.Unit, font.GdiCharSet,
                font.GdiVerticalFont);
            return maxFont;
        }

        //calculate the maximum font size for a string to fit into a rect
        public static float MaxFontSize(this Graphics g, string text, Font font, RectangleF rectangleF)
        {
            SizeF textSizeF = g.MeasureString(text, font);
            float HeightScaleRatio = rectangleF.Height / textSizeF.Height;
            float WidthScaleRatio = rectangleF.Width / textSizeF.Width;
            float ScaleRatio = (HeightScaleRatio < WidthScaleRatio)
                ? ScaleRatio = HeightScaleRatio
                : ScaleRatio = WidthScaleRatio;
            float textFontSize = font.Size * ScaleRatio;
            return textFontSize;
        }

        //calculate the maximum font for a list of string to fit into a rect
        public static Font MaxSizedFont(this Graphics g, List<string> texts, Font font, RectangleF rectangleF)
        {
            //find the largest size which still fits
            var scaleFontSize = float.MaxValue;
            foreach (var text in texts)
            {
                var textFontSize = MaxFontSize(g, text, font, rectangleF);
                if (textFontSize < scaleFontSize)
                    scaleFontSize = textFontSize;
            }

            Font maxFont = new Font(font.FontFamily, scaleFontSize, font.Style, font.Unit, font.GdiCharSet,
                font.GdiVerticalFont);
            return maxFont;
        }
    }
}